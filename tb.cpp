#include "Vcounter.h"
#include "verilated.h"
#include  "verilated_vcd_c.h"


int main(int argc, char **argv, char **env)
{

	int i;
	int clk;

	Verilated::commandArgs(argc,argv);
	Vcounter *top = new Vcounter;
	Verilated::traceEverOn(true);
	VL_PRINTF("Enabling waves...\n");
	VerilatedVcdC* tfp = new VerilatedVcdC;
        top->trace (tfp, 99);
        tfp->open ("counter.vcd");

	top -> clk = 1;
	top -> rst = 1;
	top -> cen = 0;
	top -> wen = 0;
	top -> dat = 0x55;

	for(i=0; i < 20; i++)
	{
	  top -> rst = (i < 2);
	  for(clk = 0; clk < 2; clk++)
	  {
	    tfp -> dump (2*i+clk);
	    top -> clk = ! top -> clk;
	    top -> eval();
	  }

	  top -> cen = (i > 5);
	  top -> wen = (i == 10);
	  if (Verilated::gotFinish())
		exit(0);	
	}

	tfp -> close();
	

	exit(0);
}
