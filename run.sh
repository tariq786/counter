#!/bin/bash

#cleanup
rm -rf obj_dir
rm -rf counter.vcd

#run verilator to translate verilog to c++ while including c++ tb
verilator -Wall -cc counter.v --exe tb.cpp -Wno-lint --trace --stats

#Build C++ project
make -j -C obj_dir/ -f Vcounter.mk Vcounter

#run simulation executable
obj_dir/Vcounter

#view waveforms
gtkwave counter.vcd counter.sav &



