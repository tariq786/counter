module counter #
	(parameter WIDTH = 8
	)
	(
	 input clk,
	 input rst,
	 input cen,
	 input wen,
	 input [WIDTH-1:0] dat,
	 output reg [WIDTH-1:0] o_p,
	 output reg [WIDTH-1:0] o_n
	);


      always @(posedge clk,posedge rst)
	begin
	  if(rst)
		o_p <= {WIDTH{1'b0}};
	  else
		o_p <= wen ? dat : o_p + { {WIDTH-1{1'b0}},cen};
	end		

      always @(negedge clk,posedge rst)
	begin
	  if(rst)
		o_n <= {WIDTH{1'b0}};
	  else
		o_n <= wen ? dat : o_n + { {WIDTH-1{1'b0}},cen};
	end		

endmodule

